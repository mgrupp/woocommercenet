﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//using WooCommerceNET;
//using WooCommerceNET.WooCommerce.v2;

using WooCommerceMy;

//http://bongourmet.tibuilder.net/upload/fotoarticulo/0000000116_172669202.jpg

namespace WooCommerceTest
{
	public partial class Form1 : Form
	{
        MyWooCommerce MyWooCommerce;
        private WooCategorias categorias;

        //--------------------------------------------------------------------------

        private static String Url = "https://fastadelivery.com";

        //Clave de API REST
        //Se genera en Plugins/WooCommerce/Ajustes/Avanzado/API REST
        private static String key = "ck_0c8c68d79c6fdbe2d34de5a4510bcbd240309989";
		private static String secret = "cs_ce9d90f5bb375b13bb57d644995958dbc40fd4b5";

        WooProducto productoSeleccionado = null;

        public Form1()
		{
			InitializeComponent();

            MyWooCommerce = new MyWooCommerce(Url, key, secret);

            versionTextBox.Text = MyWooCommerce.Version;

            MostrarCategorias();
        }

        private void LimpiarCategorias()
		{
            while (checkedListBoxCategorias.CheckedIndices.Count > 0)
                checkedListBoxCategorias.SetItemChecked(checkedListBoxCategorias.CheckedIndices[0], false);
        }

        private void MostrarCategorias()
        {
            categorias = MyWooCommerce.LeerTodasLasCategorias();

            checkedListBoxCategorias.Items.Clear();
            foreach(WooCategory categoria in categorias) {
                checkedListBoxCategorias.Items.Add( categoria, false );
            }
        }

        private void MarcarCategorias()
		{
            WooCategoriasProducto categoriasProducto = productoSeleccionado.oCategoriasProducto;
            
            LimpiarCategorias();

            foreach(WooCategoriaProducto categoriaProducto in categoriasProducto) {
                for (int i = 0; i < checkedListBoxCategorias.Items.Count; i++) {
                    WooCategory categoria = (WooCategory)checkedListBoxCategorias.Items[ i ];
                    if (categoria.ID == categoriaProducto.ID) {
                        checkedListBoxCategorias.SetItemChecked( i, true );
                    }
                }
            }
        }

        private void buttonProductos_Click(object sender, EventArgs e)
		{
            Cursor.Current = Cursors.WaitCursor;
            DataGridViewRow Dgvr;

            WooProductos productos = MyWooCommerce.LeerTodosLosProductos();

            dataGridViewProductos.Rows.Clear();
            foreach(WooProducto producto in productos) {
                Dgvr = dataGridViewProductos.Rows[ dataGridViewProductos.Rows.Add() ];

                Dgvr.Cells[ "colID" ].Value                 = producto.ID;
                Dgvr.Cells[ "colNombreProducto" ].Value     = producto.NOMBRELARGO;
                Dgvr.Tag = producto;
            }

            Cursor.Current = Cursors.Default;
        }

        private decimal TextoADecimal(string Texto ) {
            if (Texto.Length > 0)
                return Convert.ToDecimal( Texto );
            else
                return 0;
        }

		private void buttonActualizarProductos_Click(object sender, EventArgs e)
		{
            Cursor.Current = Cursors.WaitCursor;

            if(productoSeleccionado == null) {
                productoSeleccionado = new WooProducto();
            }

            productoSeleccionado.NOMBRELARGO        = textBoxNombreLargo.Text;
            productoSeleccionado.NOMBRECORTO        = textBoxNombreCorto.Text;
            productoSeleccionado.CARACTERISTICAS    = richTextBoxCaracteristicas.Text;
            productoSeleccionado.URLIMAGEN          = textBoxURLImagen.Text;
            productoSeleccionado.PRECIODEVENTA      = TextoADecimal(textBoxPrecioDeVenta.Text );
            productoSeleccionado.PUBLICAR           = checkBoxPublicar.Checked;
            productoSeleccionado.PRODUCTOENOFERTA   = productoEnOfertaCheckBox.Checked;
            productoSeleccionado.INICIOOFERTA       = inicioOfertadateTimePicker.Value.Date;
            productoSeleccionado.FINALOFERTA        = finalOfertaDateTimePicker.Value.Date;
            productoSeleccionado.PRECIODEOFERTA      = TextoADecimal( precioDeOfertaTextBox.Text );
            productoSeleccionado.MANEJAEXISTENCIA   = manejaExistenciaCheckBox.Checked;
            productoSeleccionado.EXISTENCIA         = (int)TextoADecimal( ExistenciaTextBox.Text );
            productoSeleccionado.TOTALDEVENTAS      = (long) TextoADecimal( totalDeVentasTextBox.Text ) ;

            productoSeleccionado.oCategoriasProducto.LimpiarCategorias();
            foreach (WooCategory categoria in checkedListBoxCategorias.CheckedItems) {
                productoSeleccionado.oCategoriasProducto.AgregarCategoria( categoria.ID );
            }

            MyWooCommerce.GrabarProducto( productoSeleccionado );

            productoSeleccionado = null;

            textBoxNombreLargo.Clear();
            textBoxNombreCorto.Clear();
            textBoxURLImagen.Clear();
            richTextBoxCaracteristicas.Clear();
            textBoxPrecioDeVenta.Clear();
            pictureBoxProducto.Image = null;
            productoEnOfertaCheckBox.Checked = false;
            inicioOfertadateTimePicker.Value = DateTime.Now;
            finalOfertaDateTimePicker.Value = DateTime.Now;
            precioDeOfertaTextBox.Clear();
            manejaExistenciaCheckBox.Checked = false;
            ExistenciaTextBox.Clear();
            totalDeVentasTextBox.Clear();


            LimpiarCategorias();

            Cursor.Current = Cursors.Default;
            return;
        }

		private void dataGridViewProductos_CellClick(object sender, DataGridViewCellEventArgs e)
		{
            Cursor.Current = Cursors.WaitCursor;

            productoSeleccionado = MyWooCommerce.LeerProductoPorId( Convert.ToInt32( dataGridViewProductos.Rows[ e.RowIndex ].Cells[ "colID" ].Value ) );

            textBoxNombreLargo.Text             = productoSeleccionado.NOMBRELARGO;
            textBoxNombreCorto.Text             = productoSeleccionado.NOMBRECORTO;
            richTextBoxCaracteristicas.Text     = productoSeleccionado.CARACTERISTICAS;
            textBoxURLImagen.Text               = productoSeleccionado.URLIMAGEN;
            pictureBoxProducto.Image            = ImagenPrincipal( productoSeleccionado.URLIMAGEN);
            textBoxPrecioDeVenta.Text           = productoSeleccionado.PRECIODEVENTA.ToString();
            checkBoxPublicar.Checked            = productoSeleccionado.PUBLICAR;
            productoEnOfertaCheckBox.Checked    = productoSeleccionado.PRODUCTOENOFERTA;
            inicioOfertadateTimePicker.Value    = productoSeleccionado.INICIOOFERTA;
            finalOfertaDateTimePicker.Value     = productoSeleccionado.FINALOFERTA;
            precioDeOfertaTextBox.Text          = productoSeleccionado.PRECIODEOFERTA.ToString();
            manejaExistenciaCheckBox.Checked    = productoSeleccionado.MANEJAEXISTENCIA;
            ExistenciaTextBox.Text              = productoSeleccionado.EXISTENCIA.ToString();
            totalDeVentasTextBox.Text           = productoSeleccionado.TOTALDEVENTAS.ToString();
            
            MarcarCategorias();

            Cursor.Current = Cursors.Default;
        }

		private void textBoxURLImagen_Validated( object sender, EventArgs e ) {
            if (productoSeleccionado != null) {
                if (!productoSeleccionado.EsValidaLaImagen( textBoxURLImagen.Text )) {
                    MessageBox.Show( "La imagen no es valida", "Mensaje", MessageBoxButtons.OK );
                    pictureBoxProducto.Image = null;
                    return;
                }
                productoSeleccionado.URLIMAGEN = textBoxURLImagen.Text;
                pictureBoxProducto.Image = ImagenPrincipal( productoSeleccionado.URLIMAGEN);
            }
        }

        public void cargarOrdenes() {
            Cursor.Current = Cursors.WaitCursor;
            
            DataGridViewRow Dgvr;
            WooOrdenes ordenes = new WooOrdenes();
            if (todoRadioButton.Checked) {
                ordenes.CargarTodo();
            }else {
                ordenes.CargarPendientes();
            }
            
            ordenesDataGridView.Rows.Clear();
            foreach (WooOrden orden in ordenes) {
                Dgvr = ordenesDataGridView.Rows[ ordenesDataGridView.Rows.Add() ];

                Dgvr.Cells[ "ID" ].Value = orden.id;
                Dgvr.Cells[ "FechaDeCreacion" ].Value = orden.FechaDeCreacion;
                Dgvr.Cells[ "NumeroOrden" ].Value = orden.numeroOrden;
                Dgvr.Cells[ "nombreYApellido" ].Value = orden.datosDeFacturacion.nombre + " " + orden.datosDeFacturacion.apellido;
                Dgvr.Cells[ "Estado" ].Value = orden.Estado;
                Dgvr.Cells[ "Total" ].Value = orden.total;

                Dgvr.Tag = orden;
            }

            Cursor.Current = Cursors.Default;

        }

		private void ordenesDataGridView_CellClick( object sender, DataGridViewCellEventArgs e ) {
            WooOrden orden = (WooOrden)ordenesDataGridView.Rows[ e.RowIndex ].Tag; ;

            DataGridViewRow Dgvr;
            detallesOrdenDataGridView.Rows.Clear();
            foreach(WooDetalleOrden detalleOrden in orden.detallesDeLaOrden) {
                Dgvr = detallesOrdenDataGridView.Rows[ detallesOrdenDataGridView.Rows.Add() ];

                Dgvr.Cells[ "idDetalleProducto" ].Value = detalleOrden.id;
                Dgvr.Cells[ "nombreDelProducto" ].Value = detalleOrden.nombreDelProducto;
                Dgvr.Cells[ "cantidad" ].Value = detalleOrden.cantidad.ToString();
                Dgvr.Cells[ "precio" ].Value = detalleOrden.precio.ToString();
                Dgvr.Cells[ "impuestoDetalleOrden" ].Value = detalleOrden.impuestoSubTotal.ToString();
                Dgvr.Cells[ "totalDetalleOrden" ].Value = detalleOrden.total.ToString();
            }
        }

		private void todoRadioButton_CheckedChanged( object sender, EventArgs e ) {
            cargarOrdenes();
        }

		private void porProcesarradioButton_CheckedChanged( object sender, EventArgs e ) {
            cargarOrdenes();
        }

		private void tabControl1_Selected( object sender, TabControlEventArgs e ) {
            TabPage current = (sender as TabControl).SelectedTab;

            if (current.Name == "OrdenesTabPage") {
                cargarOrdenes();
            }
		}


        public Image ImagenPrincipal(string URLImagen) {
            if (URLImagen.Length == 0) return null;

            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try {
                request = (HttpWebRequest)WebRequest.Create( URLImagen );
                response = (HttpWebResponse)request.GetResponse();

                Stream StreamImagen = response.GetResponseStream();
                return Image.FromStream( StreamImagen );
            } catch (Exception e) {
                return null;
            } finally {
                if (request != null) request.Abort();
                if (response != null) response.Close();
            }
        }

		private void dataGridViewProductos_CellContentClick( object sender, DataGridViewCellEventArgs e ) {

		}
	}
}
