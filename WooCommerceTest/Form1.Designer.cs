﻿namespace WooCommerceTest
{
	partial class Form1
	{
		/// <summary>
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.ordenesTabControl = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.versionTextBox = new System.Windows.Forms.TextBox();
			this.dataGridViewProductos = new System.Windows.Forms.DataGridView();
			this.colID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colNombreProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.totalDeVentasTextBox = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.ExistenciaTextBox = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.manejaExistenciaCheckBox = new System.Windows.Forms.CheckBox();
			this.buttonProductos = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.precioDeOfertaTextBox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.finalOfertaDateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.inicioOfertadateTimePicker = new System.Windows.Forms.DateTimePicker();
			this.productoEnOfertaCheckBox = new System.Windows.Forms.CheckBox();
			this.richTextBoxCaracteristicas = new System.Windows.Forms.RichTextBox();
			this.checkBoxPublicar = new System.Windows.Forms.CheckBox();
			this.buttonActualizarProductos = new System.Windows.Forms.Button();
			this.textBoxPrecioDeVenta = new System.Windows.Forms.TextBox();
			this.pictureBoxProducto = new System.Windows.Forms.PictureBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.textBoxNombreLargo = new System.Windows.Forms.TextBox();
			this.checkedListBoxCategorias = new System.Windows.Forms.CheckedListBox();
			this.textBoxNombreCorto = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxURLImagen = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.OrdenesTabPage = new System.Windows.Forms.TabPage();
			this.porProcesarradioButton = new System.Windows.Forms.RadioButton();
			this.todoRadioButton = new System.Windows.Forms.RadioButton();
			this.label12 = new System.Windows.Forms.Label();
			this.detallesOrdenDataGridView = new System.Windows.Forms.DataGridView();
			this.idDetalleProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nombreDelProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.impuestoDetalleOrden = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.totalDetalleOrden = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ordenesDataGridView = new System.Windows.Forms.DataGridView();
			this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FechaDeCreacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.nombreYApellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.NumeroOrden = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ordenesTabControl.SuspendLayout();
			this.tabPage1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProductos)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxProducto)).BeginInit();
			this.OrdenesTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.detallesOrdenDataGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ordenesDataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// ordenesTabControl
			// 
			this.ordenesTabControl.Controls.Add(this.tabPage1);
			this.ordenesTabControl.Controls.Add(this.OrdenesTabPage);
			this.ordenesTabControl.Location = new System.Drawing.Point(12, 12);
			this.ordenesTabControl.Name = "ordenesTabControl";
			this.ordenesTabControl.SelectedIndex = 0;
			this.ordenesTabControl.Size = new System.Drawing.Size(1203, 591);
			this.ordenesTabControl.TabIndex = 0;
			this.ordenesTabControl.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.versionTextBox);
			this.tabPage1.Controls.Add(this.dataGridViewProductos);
			this.tabPage1.Controls.Add(this.groupBox2);
			this.tabPage1.Controls.Add(this.buttonProductos);
			this.tabPage1.Controls.Add(this.groupBox1);
			this.tabPage1.Controls.Add(this.richTextBoxCaracteristicas);
			this.tabPage1.Controls.Add(this.checkBoxPublicar);
			this.tabPage1.Controls.Add(this.buttonActualizarProductos);
			this.tabPage1.Controls.Add(this.textBoxPrecioDeVenta);
			this.tabPage1.Controls.Add(this.pictureBoxProducto);
			this.tabPage1.Controls.Add(this.label7);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.label6);
			this.tabPage1.Controls.Add(this.textBoxNombreLargo);
			this.tabPage1.Controls.Add(this.checkedListBoxCategorias);
			this.tabPage1.Controls.Add(this.textBoxNombreCorto);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.label5);
			this.tabPage1.Controls.Add(this.textBoxURLImagen);
			this.tabPage1.Controls.Add(this.label4);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(1195, 565);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Productos";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// versionTextBox
			// 
			this.versionTextBox.Location = new System.Drawing.Point(18, 524);
			this.versionTextBox.Name = "versionTextBox";
			this.versionTextBox.Size = new System.Drawing.Size(118, 20);
			this.versionTextBox.TabIndex = 12;
			// 
			// dataGridViewProductos
			// 
			this.dataGridViewProductos.AllowUserToAddRows = false;
			this.dataGridViewProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colID,
            this.colNombreProducto});
			this.dataGridViewProductos.Location = new System.Drawing.Point(18, 11);
			this.dataGridViewProductos.Name = "dataGridViewProductos";
			this.dataGridViewProductos.ReadOnly = true;
			this.dataGridViewProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.dataGridViewProductos.Size = new System.Drawing.Size(414, 495);
			this.dataGridViewProductos.TabIndex = 9;
			this.dataGridViewProductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProductos_CellClick);
			this.dataGridViewProductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProductos_CellContentClick);
			// 
			// colID
			// 
			this.colID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.colID.HeaderText = "ID";
			this.colID.Name = "colID";
			this.colID.ReadOnly = true;
			this.colID.Width = 43;
			// 
			// colNombreProducto
			// 
			this.colNombreProducto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.colNombreProducto.HeaderText = "Nombre del producto";
			this.colNombreProducto.Name = "colNombreProducto";
			this.colNombreProducto.ReadOnly = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.totalDeVentasTextBox);
			this.groupBox2.Controls.Add(this.label11);
			this.groupBox2.Controls.Add(this.ExistenciaTextBox);
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.manejaExistenciaCheckBox);
			this.groupBox2.Location = new System.Drawing.Point(927, 410);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(240, 134);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Manejo de existencias";
			// 
			// totalDeVentasTextBox
			// 
			this.totalDeVentasTextBox.Location = new System.Drawing.Point(13, 108);
			this.totalDeVentasTextBox.Name = "totalDeVentasTextBox";
			this.totalDeVentasTextBox.Size = new System.Drawing.Size(118, 20);
			this.totalDeVentasTextBox.TabIndex = 4;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(13, 92);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(81, 13);
			this.label11.TabIndex = 3;
			this.label11.Text = "Total de ventas";
			// 
			// ExistenciaTextBox
			// 
			this.ExistenciaTextBox.Location = new System.Drawing.Point(13, 62);
			this.ExistenciaTextBox.Name = "ExistenciaTextBox";
			this.ExistenciaTextBox.Size = new System.Drawing.Size(118, 20);
			this.ExistenciaTextBox.TabIndex = 2;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(13, 46);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(55, 13);
			this.label10.TabIndex = 1;
			this.label10.Text = "Existencia";
			// 
			// manejaExistenciaCheckBox
			// 
			this.manejaExistenciaCheckBox.AutoSize = true;
			this.manejaExistenciaCheckBox.Location = new System.Drawing.Point(13, 24);
			this.manejaExistenciaCheckBox.Name = "manejaExistenciaCheckBox";
			this.manejaExistenciaCheckBox.Size = new System.Drawing.Size(116, 17);
			this.manejaExistenciaCheckBox.TabIndex = 0;
			this.manejaExistenciaCheckBox.Text = "Maneja existencias";
			this.manejaExistenciaCheckBox.UseVisualStyleBackColor = true;
			// 
			// buttonProductos
			// 
			this.buttonProductos.Location = new System.Drawing.Point(291, 515);
			this.buttonProductos.Name = "buttonProductos";
			this.buttonProductos.Size = new System.Drawing.Size(141, 23);
			this.buttonProductos.TabIndex = 8;
			this.buttonProductos.Text = "Cargar lista de productos";
			this.buttonProductos.UseVisualStyleBackColor = true;
			this.buttonProductos.Click += new System.EventHandler(this.buttonProductos_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.precioDeOfertaTextBox);
			this.groupBox1.Controls.Add(this.label9);
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.finalOfertaDateTimePicker);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.inicioOfertadateTimePicker);
			this.groupBox1.Controls.Add(this.productoEnOfertaCheckBox);
			this.groupBox1.Location = new System.Drawing.Point(927, 210);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(240, 183);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Oferta";
			// 
			// precioDeOfertaTextBox
			// 
			this.precioDeOfertaTextBox.Location = new System.Drawing.Point(13, 150);
			this.precioDeOfertaTextBox.Name = "precioDeOfertaTextBox";
			this.precioDeOfertaTextBox.Size = new System.Drawing.Size(118, 20);
			this.precioDeOfertaTextBox.TabIndex = 6;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(13, 134);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(82, 13);
			this.label9.TabIndex = 5;
			this.label9.Text = "Precio de oferta";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(13, 87);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(62, 13);
			this.label8.TabIndex = 3;
			this.label8.Text = "Finalización";
			// 
			// finalOfertaDateTimePicker
			// 
			this.finalOfertaDateTimePicker.Location = new System.Drawing.Point(13, 103);
			this.finalOfertaDateTimePicker.Name = "finalOfertaDateTimePicker";
			this.finalOfertaDateTimePicker.Size = new System.Drawing.Size(200, 20);
			this.finalOfertaDateTimePicker.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 42);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(32, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Inicio";
			// 
			// inicioOfertadateTimePicker
			// 
			this.inicioOfertadateTimePicker.Location = new System.Drawing.Point(13, 58);
			this.inicioOfertadateTimePicker.Name = "inicioOfertadateTimePicker";
			this.inicioOfertadateTimePicker.Size = new System.Drawing.Size(200, 20);
			this.inicioOfertadateTimePicker.TabIndex = 2;
			// 
			// productoEnOfertaCheckBox
			// 
			this.productoEnOfertaCheckBox.AutoSize = true;
			this.productoEnOfertaCheckBox.Location = new System.Drawing.Point(13, 19);
			this.productoEnOfertaCheckBox.Name = "productoEnOfertaCheckBox";
			this.productoEnOfertaCheckBox.Size = new System.Drawing.Size(69, 17);
			this.productoEnOfertaCheckBox.TabIndex = 0;
			this.productoEnOfertaCheckBox.Text = "En oferta";
			this.productoEnOfertaCheckBox.UseVisualStyleBackColor = true;
			// 
			// richTextBoxCaracteristicas
			// 
			this.richTextBoxCaracteristicas.Location = new System.Drawing.Point(482, 78);
			this.richTextBoxCaracteristicas.Name = "richTextBoxCaracteristicas";
			this.richTextBoxCaracteristicas.Size = new System.Drawing.Size(506, 61);
			this.richTextBoxCaracteristicas.TabIndex = 2;
			this.richTextBoxCaracteristicas.Text = "";
			// 
			// checkBoxPublicar
			// 
			this.checkBoxPublicar.AutoSize = true;
			this.checkBoxPublicar.Location = new System.Drawing.Point(482, 249);
			this.checkBoxPublicar.Name = "checkBoxPublicar";
			this.checkBoxPublicar.Size = new System.Drawing.Size(64, 17);
			this.checkBoxPublicar.TabIndex = 5;
			this.checkBoxPublicar.Text = "Publicar";
			this.checkBoxPublicar.UseVisualStyleBackColor = true;
			// 
			// buttonActualizarProductos
			// 
			this.buttonActualizarProductos.Location = new System.Drawing.Point(480, 515);
			this.buttonActualizarProductos.Name = "buttonActualizarProductos";
			this.buttonActualizarProductos.Size = new System.Drawing.Size(220, 23);
			this.buttonActualizarProductos.TabIndex = 7;
			this.buttonActualizarProductos.Text = "Actualizar Productos";
			this.buttonActualizarProductos.UseVisualStyleBackColor = true;
			this.buttonActualizarProductos.Click += new System.EventHandler(this.buttonActualizarProductos_Click);
			// 
			// textBoxPrecioDeVenta
			// 
			this.textBoxPrecioDeVenta.Location = new System.Drawing.Point(482, 210);
			this.textBoxPrecioDeVenta.Name = "textBoxPrecioDeVenta";
			this.textBoxPrecioDeVenta.Size = new System.Drawing.Size(118, 20);
			this.textBoxPrecioDeVenta.TabIndex = 4;
			// 
			// pictureBoxProducto
			// 
			this.pictureBoxProducto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pictureBoxProducto.Location = new System.Drawing.Point(1007, 31);
			this.pictureBoxProducto.Name = "pictureBoxProducto";
			this.pictureBoxProducto.Size = new System.Drawing.Size(160, 151);
			this.pictureBoxProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBoxProducto.TabIndex = 5;
			this.pictureBoxProducto.TabStop = false;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(482, 194);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(82, 13);
			this.label7.TabIndex = 8;
			this.label7.Text = "Precio de venta";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(482, 15);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 13);
			this.label2.TabIndex = 0;
			this.label2.Text = "Nombre largo";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(482, 62);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(78, 13);
			this.label6.TabIndex = 4;
			this.label6.Text = "Características";
			// 
			// textBoxNombreLargo
			// 
			this.textBoxNombreLargo.Location = new System.Drawing.Point(482, 31);
			this.textBoxNombreLargo.Name = "textBoxNombreLargo";
			this.textBoxNombreLargo.Size = new System.Drawing.Size(251, 20);
			this.textBoxNombreLargo.TabIndex = 0;
			// 
			// checkedListBoxCategorias
			// 
			this.checkedListBoxCategorias.CheckOnClick = true;
			this.checkedListBoxCategorias.FormattingEnabled = true;
			this.checkedListBoxCategorias.Location = new System.Drawing.Point(480, 307);
			this.checkedListBoxCategorias.Name = "checkedListBoxCategorias";
			this.checkedListBoxCategorias.Size = new System.Drawing.Size(220, 199);
			this.checkedListBoxCategorias.TabIndex = 6;
			// 
			// textBoxNombreCorto
			// 
			this.textBoxNombreCorto.Location = new System.Drawing.Point(737, 31);
			this.textBoxNombreCorto.Name = "textBoxNombreCorto";
			this.textBoxNombreCorto.Size = new System.Drawing.Size(251, 20);
			this.textBoxNombreCorto.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(482, 146);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(67, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "URL Imagen";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(734, 15);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(71, 13);
			this.label5.TabIndex = 1;
			this.label5.Text = "Nombre corto";
			// 
			// textBoxURLImagen
			// 
			this.textBoxURLImagen.Location = new System.Drawing.Point(482, 162);
			this.textBoxURLImagen.Name = "textBoxURLImagen";
			this.textBoxURLImagen.Size = new System.Drawing.Size(506, 20);
			this.textBoxURLImagen.TabIndex = 3;
			this.textBoxURLImagen.Validated += new System.EventHandler(this.textBoxURLImagen_Validated);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(482, 289);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(57, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Categorias";
			// 
			// OrdenesTabPage
			// 
			this.OrdenesTabPage.Controls.Add(this.porProcesarradioButton);
			this.OrdenesTabPage.Controls.Add(this.todoRadioButton);
			this.OrdenesTabPage.Controls.Add(this.label12);
			this.OrdenesTabPage.Controls.Add(this.detallesOrdenDataGridView);
			this.OrdenesTabPage.Controls.Add(this.ordenesDataGridView);
			this.OrdenesTabPage.Location = new System.Drawing.Point(4, 22);
			this.OrdenesTabPage.Name = "OrdenesTabPage";
			this.OrdenesTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.OrdenesTabPage.Size = new System.Drawing.Size(1195, 565);
			this.OrdenesTabPage.TabIndex = 1;
			this.OrdenesTabPage.Text = "Órdenes";
			this.OrdenesTabPage.UseVisualStyleBackColor = true;
			// 
			// porProcesarradioButton
			// 
			this.porProcesarradioButton.AutoSize = true;
			this.porProcesarradioButton.Location = new System.Drawing.Point(72, 524);
			this.porProcesarradioButton.Name = "porProcesarradioButton";
			this.porProcesarradioButton.Size = new System.Drawing.Size(85, 17);
			this.porProcesarradioButton.TabIndex = 8;
			this.porProcesarradioButton.Text = "Por procesar";
			this.porProcesarradioButton.UseVisualStyleBackColor = true;
			this.porProcesarradioButton.CheckedChanged += new System.EventHandler(this.porProcesarradioButton_CheckedChanged);
			// 
			// todoRadioButton
			// 
			this.todoRadioButton.AutoSize = true;
			this.todoRadioButton.Checked = true;
			this.todoRadioButton.Location = new System.Drawing.Point(16, 524);
			this.todoRadioButton.Name = "todoRadioButton";
			this.todoRadioButton.Size = new System.Drawing.Size(50, 17);
			this.todoRadioButton.TabIndex = 7;
			this.todoRadioButton.TabStop = true;
			this.todoRadioButton.Text = "Todo";
			this.todoRadioButton.UseVisualStyleBackColor = true;
			this.todoRadioButton.CheckedChanged += new System.EventHandler(this.todoRadioButton_CheckedChanged);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(593, 17);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(96, 13);
			this.label12.TabIndex = 6;
			this.label12.Text = "Detalle de la orden";
			// 
			// detallesOrdenDataGridView
			// 
			this.detallesOrdenDataGridView.AllowUserToAddRows = false;
			this.detallesOrdenDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.detallesOrdenDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDetalleProducto,
            this.nombreDelProducto,
            this.cantidad,
            this.precio,
            this.impuestoDetalleOrden,
            this.totalDetalleOrden});
			this.detallesOrdenDataGridView.Location = new System.Drawing.Point(596, 33);
			this.detallesOrdenDataGridView.Name = "detallesOrdenDataGridView";
			this.detallesOrdenDataGridView.ReadOnly = true;
			this.detallesOrdenDataGridView.RowHeadersVisible = false;
			this.detallesOrdenDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.detallesOrdenDataGridView.Size = new System.Drawing.Size(549, 479);
			this.detallesOrdenDataGridView.TabIndex = 5;
			// 
			// idDetalleProducto
			// 
			this.idDetalleProducto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.idDetalleProducto.HeaderText = "ID";
			this.idDetalleProducto.Name = "idDetalleProducto";
			this.idDetalleProducto.ReadOnly = true;
			this.idDetalleProducto.Width = 43;
			// 
			// nombreDelProducto
			// 
			this.nombreDelProducto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.nombreDelProducto.HeaderText = "Nombre del prodcuto";
			this.nombreDelProducto.Name = "nombreDelProducto";
			this.nombreDelProducto.ReadOnly = true;
			// 
			// cantidad
			// 
			this.cantidad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.cantidad.HeaderText = "Cantidad";
			this.cantidad.Name = "cantidad";
			this.cantidad.ReadOnly = true;
			this.cantidad.Width = 74;
			// 
			// precio
			// 
			this.precio.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.precio.HeaderText = "Precio";
			this.precio.Name = "precio";
			this.precio.ReadOnly = true;
			this.precio.Width = 62;
			// 
			// impuestoDetalleOrden
			// 
			this.impuestoDetalleOrden.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.impuestoDetalleOrden.HeaderText = "Impuesto";
			this.impuestoDetalleOrden.Name = "impuestoDetalleOrden";
			this.impuestoDetalleOrden.ReadOnly = true;
			this.impuestoDetalleOrden.Width = 75;
			// 
			// totalDetalleOrden
			// 
			this.totalDetalleOrden.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
			this.totalDetalleOrden.HeaderText = "Total";
			this.totalDetalleOrden.Name = "totalDetalleOrden";
			this.totalDetalleOrden.ReadOnly = true;
			this.totalDetalleOrden.Width = 56;
			// 
			// ordenesDataGridView
			// 
			this.ordenesDataGridView.AllowUserToAddRows = false;
			this.ordenesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.ordenesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.FechaDeCreacion,
            this.nombreYApellido,
            this.NumeroOrden,
            this.Estado,
            this.Total});
			this.ordenesDataGridView.Location = new System.Drawing.Point(6, 17);
			this.ordenesDataGridView.Name = "ordenesDataGridView";
			this.ordenesDataGridView.ReadOnly = true;
			this.ordenesDataGridView.RowHeadersVisible = false;
			this.ordenesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.ordenesDataGridView.Size = new System.Drawing.Size(549, 495);
			this.ordenesDataGridView.TabIndex = 3;
			this.ordenesDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ordenesDataGridView_CellClick);
			// 
			// ID
			// 
			this.ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
			this.ID.HeaderText = "ID";
			this.ID.Name = "ID";
			this.ID.ReadOnly = true;
			this.ID.Width = 43;
			// 
			// FechaDeCreacion
			// 
			this.FechaDeCreacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
			this.FechaDeCreacion.HeaderText = "Fecha";
			this.FechaDeCreacion.Name = "FechaDeCreacion";
			this.FechaDeCreacion.ReadOnly = true;
			this.FechaDeCreacion.Width = 62;
			// 
			// nombreYApellido
			// 
			this.nombreYApellido.HeaderText = "Nombre";
			this.nombreYApellido.Name = "nombreYApellido";
			this.nombreYApellido.ReadOnly = true;
			// 
			// NumeroOrden
			// 
			this.NumeroOrden.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.NumeroOrden.HeaderText = "Orden";
			this.NumeroOrden.Name = "NumeroOrden";
			this.NumeroOrden.ReadOnly = true;
			// 
			// Estado
			// 
			this.Estado.HeaderText = "Estado";
			this.Estado.Name = "Estado";
			this.Estado.ReadOnly = true;
			// 
			// Total
			// 
			this.Total.HeaderText = "Total";
			this.Total.Name = "Total";
			this.Total.ReadOnly = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1230, 616);
			this.Controls.Add(this.ordenesTabControl);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "WooCommerce";
			this.ordenesTabControl.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProductos)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxProducto)).EndInit();
			this.OrdenesTabPage.ResumeLayout(false);
			this.OrdenesTabPage.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.detallesOrdenDataGridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ordenesDataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.TabControl ordenesTabControl;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage OrdenesTabPage;
		private System.Windows.Forms.Button buttonProductos;
		private System.Windows.Forms.DataGridViewTextBoxColumn colNombreProducto;
		private System.Windows.Forms.DataGridViewTextBoxColumn colID;
		private System.Windows.Forms.DataGridView dataGridViewProductos;
		private System.Windows.Forms.Button buttonActualizarProductos;
		private System.Windows.Forms.PictureBox pictureBoxProducto;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox textBoxNombreLargo;
		private System.Windows.Forms.CheckedListBox checkedListBoxCategorias;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxURLImagen;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox textBoxNombreCorto;
		private System.Windows.Forms.RichTextBox richTextBoxCaracteristicas;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxPrecioDeVenta;
		private System.Windows.Forms.CheckBox checkBoxPublicar;
		private System.Windows.Forms.CheckBox productoEnOfertaCheckBox;
		private System.Windows.Forms.DateTimePicker inicioOfertadateTimePicker;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DateTimePicker finalOfertaDateTimePicker;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox precioDeOfertaTextBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox manejaExistenciaCheckBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox ExistenciaTextBox;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox totalDeVentasTextBox;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.DataGridView ordenesDataGridView;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.DataGridView detallesOrdenDataGridView;
		private System.Windows.Forms.DataGridViewTextBoxColumn idDetalleProducto;
		private System.Windows.Forms.DataGridViewTextBoxColumn nombreDelProducto;
		private System.Windows.Forms.DataGridViewTextBoxColumn cantidad;
		private System.Windows.Forms.DataGridViewTextBoxColumn precio;
		private System.Windows.Forms.DataGridViewTextBoxColumn impuestoDetalleOrden;
		private System.Windows.Forms.DataGridViewTextBoxColumn totalDetalleOrden;
		private System.Windows.Forms.RadioButton porProcesarradioButton;
		private System.Windows.Forms.RadioButton todoRadioButton;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID;
		private System.Windows.Forms.DataGridViewTextBoxColumn FechaDeCreacion;
		private System.Windows.Forms.DataGridViewTextBoxColumn nombreYApellido;
		private System.Windows.Forms.DataGridViewTextBoxColumn NumeroOrden;
		private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
		private System.Windows.Forms.DataGridViewTextBoxColumn Total;
		private System.Windows.Forms.TextBox versionTextBox;
	}
}

