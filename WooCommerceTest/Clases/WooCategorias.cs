﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooCategorias : WooObject, IEnumerable {
		
		public List<WooCategory> oCategorias { get; set; }

		public WooCategory oCategoria { get; set; }

		public WooCategorias() {
			oCategoria = null;
			oCategorias = new List<WooCategory>();
			//CargarTodo();
		}

		/*
		private void CargarTodo() {
			List<ProductCategory> ProductCategorys = (wcObject.Category.GetAll( new Dictionary<string, string>() )).Result;

			foreach (ProductCategory productCategory in ProductCategorys) {
				oCategorias.Add( new WooCategory( productCategory ) );
			}
		}
		*/
		public IEnumerator GetEnumerator() {
			return oCategorias.GetEnumerator();
		}
	}
}
