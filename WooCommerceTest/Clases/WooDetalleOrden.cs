﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooDetalleOrden {
		public int id;
		public string nombreDelProducto = "";
		public decimal cantidad;
		public decimal subTotal;
		public decimal impuestoSubTotal;
		public decimal total;
		public decimal precio;

		OrderLineItem orderLineItem;

		public WooDetalleOrden( OrderLineItem orderLineItem ) {
			id = orderLineItem.id.Value;
			nombreDelProducto = orderLineItem.name;
			cantidad = orderLineItem.quantity.Value;
			subTotal = orderLineItem.subtotal.Value;
			impuestoSubTotal = orderLineItem.subtotal_tax.Value;
			total = orderLineItem.total.Value;
			precio = orderLineItem.price.Value;
		}
	}
}
