﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooProductos : WooObject, IEnumerable 
	{
		public List<WooProducto> oProductos;

		public WooProductos() {
			oProductos = new List<WooProducto>();
		}

		public IEnumerator GetEnumerator() {
			return oProductos.GetEnumerator();
		}
	}
}
