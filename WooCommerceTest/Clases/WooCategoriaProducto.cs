﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {

	public class WooCategoriaProducto : WooObject 
	{
		public WooCategoriaProducto() 
		{
			ID = 0;
			NOMBRECATEGORIA = "";
		}

		public int ID { get; set; }

		public string NOMBRECATEGORIA { get; set; }
	}
}
