﻿// Documentacion
//	https://github.com/XiaoFaye/WooCommerce.NET
//	https://woocommerce.github.io/woocommerce-rest-api-docs/#product-properties

using System;
using System.Collections.Generic;
using WooCommerceNET;
using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy
{
    public class MyWooCommerce : WooObject
    {
        public static string Version = "1.0";
        private static string WooCommerceAPIVersion = "//wp-json/wc/v2/";
        private RestAPI restAPI = null;

        public MyWooCommerce(string Url, string key, string secret)
        {
            restAPI = new RestAPI(Url + WooCommerceAPIVersion, key, secret);

            wcObject = new WCObject(restAPI);
        }

        #region Producto

        public void GrabarProducto(WooProducto oWooProducto)
        {
            oWooProducto.oProduct.name = oWooProducto.NOMBRELARGO;
            oWooProducto.oProduct.short_description = oWooProducto.NOMBRECORTO;
            oWooProducto.oProduct.description = oWooProducto.CARACTERISTICAS;
            oWooProducto.oProduct.regular_price = oWooProducto.PRECIODEVENTA;
            oWooProducto.oProduct.status = oWooProducto.PUBLICAR ? "publish" : "draft";
            oWooProducto.oProduct.on_sale = oWooProducto.PRODUCTOENOFERTA;
            oWooProducto.oProduct.date_on_sale_from_gmt = oWooProducto.INICIOOFERTA;
            oWooProducto.oProduct.date_on_sale_to_gmt = oWooProducto.FINALOFERTA;
            oWooProducto.oProduct.sale_price = oWooProducto.PRECIODEOFERTA;
            oWooProducto.oProduct.manage_stock = oWooProducto.MANEJAEXISTENCIA;
            oWooProducto.oProduct.stock_quantity = oWooProducto.EXISTENCIA;
            oWooProducto.oProduct.total_sales = oWooProducto.TOTALDEVENTAS;

            if (oWooProducto.oProductImage.src != oWooProducto.URLIMAGEN)
            {
                if (oWooProducto.oProductImage.id > 0)
                {
                    oWooProducto.oProduct.images.Remove(oWooProducto.oProductImage);
                    oWooProducto.oProductImage = new ProductImage();
                }
                if (oWooProducto.URLIMAGEN.Length > 0)
                {
                    oWooProducto.oProductImage.src = oWooProducto.URLIMAGEN;
                    oWooProducto.oProduct.images = new List<ProductImage>();
                    oWooProducto.oProduct.images.Insert(0, oWooProducto.oProductImage);
                }
            }

            if (oWooProducto.oProduct.categories == null)
                oWooProducto.oProduct.categories = new List<ProductCategoryLine>();
            else
                oWooProducto.oProduct.categories.Clear();

            foreach (WooCategoriaProducto categoriaProducto in oWooProducto.oCategoriasProducto)
            {
                ProductCategoryLine productCategoryLine = new ProductCategoryLine() { id = categoriaProducto.ID };
                oWooProducto.oProduct.categories.Add(productCategoryLine);
            }

            if (oWooProducto.ID == 0)
            {
                oWooProducto.oProduct = wcObject.Product.Add(oWooProducto.oProduct ).GetAwaiter().GetResult();
                oWooProducto.ID = oWooProducto.oProduct.id.Value;
            }
            else
            {
                oWooProducto.oProduct = (wcObject.Product.Update(oWooProducto.ID, oWooProducto.oProduct, new Dictionary<string, string>())).Result;
            }
        }

        public WooProducto LeerProductoPorId(int id)
        {
            WooProducto oWooProducto = null;
            Product oProduct = (wcObject.Product.Get(id)).Result;
            if (oProduct != null)
                oWooProducto = ImportarProducto(oProduct);

            return oWooProducto;
        }

        public WooProducto ImportarProducto(Product oProduct)
        {
            WooProducto oWooProducto = new WooProducto();
            oWooProducto.oProduct = oProduct;
            oWooProducto.ID = oProduct.id.Value;
            oWooProducto.NOMBRELARGO = oProduct.name;
            oWooProducto.NOMBRECORTO = (oProduct.short_description.Replace("<p>", "")).Replace("</p>", ""); ;
            oWooProducto.CARACTERISTICAS = (oProduct.description.Replace("<p>", "")).Replace("</p>", "");
            oWooProducto.oCategorias = oProduct.categories;
            oWooProducto.PRECIODEVENTA = oProduct.regular_price.Value;
            oWooProducto.PUBLICAR = (oProduct.status == "publish");
            oWooProducto.PRODUCTOENOFERTA = oProduct.on_sale.Value;
            if (oProduct.date_on_sale_from_gmt != null && oProduct.date_on_sale_from_gmt != DateTime.MinValue)
                oWooProducto.INICIOOFERTA = oProduct.date_on_sale_from_gmt.Value;
            if (oProduct.date_on_sale_to_gmt != null && oProduct.date_on_sale_to_gmt != DateTime.MinValue)
                oWooProducto.FINALOFERTA = oProduct.date_on_sale_to_gmt.Value;
            if (oProduct.sale_price != null)
                oWooProducto.PRECIODEOFERTA = oProduct.sale_price.Value;
            oWooProducto.MANEJAEXISTENCIA = oProduct.manage_stock.Value;
            if (oProduct.stock_quantity != null)
                oWooProducto.EXISTENCIA = oProduct.stock_quantity.Value;
            if (oProduct.total_sales != null)
                oWooProducto.TOTALDEVENTAS = oProduct.total_sales.Value;

            oWooProducto.oCategoriasProducto = LeerTodasLasCategoriasDelProducto( oWooProducto );

            if (oProduct.images.Count > 0)
            {
                oWooProducto.oProductImage = oProduct.images[0];
                oWooProducto.URLIMAGEN = oWooProducto.oProductImage.src;
            }
            else
            {
                oWooProducto.oProductImage = new ProductImage();
            }
            return oWooProducto;
        }

        #endregion

        #region Productos
        public WooProductos LeerTodosLosProductos()
        {
            // Parametros del Dictionary	{ "include", "10, 11, 12, 13, 14, 15" },
            //								{ "per_page", "15" }
            // List<Product> productos = (wcObject.Product.GetAll( new Dictionary<string, string>() { {"include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" } })).Result;
            //
            WooProductos oProductos = new WooProductos();

            List<Product> products = ( wcObject.Product.GetAll( new Dictionary<string, string>() ) ).Result;
            if (products != null)
            {
                foreach (Product product in products)
                {
                    oProductos.oProductos.Add( ImportarProducto(product) );
                }
            }
            return oProductos;
        }
        #endregion

        #region Categoria

        public void GrabarCategoria(WooCategory pCategory) {
            pCategory.oProductCategory.name = pCategory.NOMBRECATEGORIA;

            if (pCategory.ID == 0) pCategory.oProductCategory = wcObject.Category.Add( pCategory.oProductCategory ).GetAwaiter().GetResult();
            else pCategory.oProductCategory = (wcObject.Category.Update( pCategory.ID, pCategory.oProductCategory, new Dictionary<string, string>() )).Result;
        }

        public WooCategory ImportarCategoria( ProductCategory oProductCategory )
        {
            WooCategory wooCategory = new WooCategory() 
            {
                oProductCategory = oProductCategory,
                ID = oProductCategory.id.Value,
                NOMBRECATEGORIA = oProductCategory.name,
                IDCATEGORIAPADRE = (oProductCategory.parent != null) ? oProductCategory.parent.Value : 0,
                MODOVISUALIZACION = oProductCategory.display,
            };
            return wooCategory;
        }
        #endregion

        #region Categorias
        public WooCategorias LeerTodasLasCategorias() {
            WooCategorias oCategorias = new WooCategorias();
            List<ProductCategory> ProductCategorys = (wcObject.Category.GetAll( new Dictionary<string, string>() )).Result;

            foreach (ProductCategory productCategory in ProductCategorys) {
                oCategorias.oCategorias.Add( ImportarCategoria( productCategory ) );
            }
            return oCategorias;
        }
        #endregion

        #region CategoriaProducto
        public WooCategoriaProducto ImportarCategoriaProducto( ProductCategoryLine pProductCategoryLine ) 
        {
            WooCategoriaProducto oCategoriaProducto = new WooCategoriaProducto() {
                ID = pProductCategoryLine.id.Value,
                NOMBRECATEGORIA = pProductCategoryLine.name,
            };
            return oCategoriaProducto;
        }
        #endregion

        #region CategoriasProducto
        public WooCategoriasProducto LeerTodasLasCategoriasDelProducto(WooProducto oProducto) 
        {
            WooCategoriasProducto oCategoriasProducto = new WooCategoriasProducto();
            foreach (ProductCategoryLine productCategory in oProducto.oProduct.categories)
            {
                oCategoriasProducto.oCategoriasProducto.Add( ImportarCategoriaProducto( productCategory ) );
            }
            return oCategoriasProducto;
        }
        #endregion
    }
}
