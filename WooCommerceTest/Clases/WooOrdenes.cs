﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooOrdenes : WooObject {
		private List<WooOrden> ordenes = new List<WooOrden>();

		public void CargarTodo() {
			//
			// Parametros del Dictionary	{ "include", "10, 11, 12, 13, 14, 15" },
			//								{ "per_page", "15" }
			// List<Product> productos = (wcObject.Product.GetAll( new Dictionary<string, string>() { {"include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" } })).Result;
			//
			List<Order> orders = (wcObject.Order.GetAll( new Dictionary<string, string>() )).Result;
			if (orders != null) {
				foreach (Order order in orders) {
					ordenes.Add( new WooOrden( order ) );
				}
			}
		}

		public void CargarPendientes() {
			//
			// Parametros del Dictionary	{ "include", "10, 11, 12, 13, 14, 15" },
			//								{ "per_page", "15" }
			// List<Product> productos = (wcObject.Product.GetAll( new Dictionary<string, string>() { {"include", "10, 11, 12, 13, 14, 15" }, { "per_page", "15" } })).Result;
			//

			List<Order> orders = (wcObject.Order.GetAll( new Dictionary<string, string>() { { "status", "processing" } } )).Result;
			if (orders != null) {
				foreach (Order order in orders) {
					ordenes.Add( new WooOrden( order ) );
				}
			}
		}

		public IEnumerator GetEnumerator() {
			return ordenes.GetEnumerator();
		}
	}
}
