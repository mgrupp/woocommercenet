﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooDetallesDeLaOrden : WooObject {
		private List<WooDetalleOrden> detallesDeLaOrden = null;

		public WooDetallesDeLaOrden( Order order ) {
			detallesDeLaOrden = new List<WooDetalleOrden>();

			foreach (OrderLineItem orderLineItem in order.line_items) {
				detallesDeLaOrden.Add( new WooDetalleOrden( orderLineItem ) );
			}
		}

		public IEnumerator GetEnumerator() {
			return detallesDeLaOrden.GetEnumerator();
		}
	}
}
