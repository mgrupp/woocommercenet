﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy
{
    public class WooProducto : WooObject 
	{
		public WooProducto()
		{
			ID = 0;
			NOMBRELARGO = "";
			NOMBRECORTO = "";
			CARACTERISTICAS = "";
			PRECIODEVENTA = 0;
			URLIMAGEN = "";
			PUBLICAR = false;
			PRODUCTOENOFERTA= false;
			INICIOOFERTA = DateTime.Now.Date;
			FINALOFERTA = DateTime.Now.Date;
			PRECIODEOFERTA = 0;
			MANEJAEXISTENCIA = false;
			EXISTENCIA = 0;
			TOTALDEVENTAS = 0;

			oProduct = new Product();
			oProductImage = new ProductImage();
			oCategoriasProducto = new WooCategoriasProducto();
		}

		public int ID { get; set; }

		/// <summary>
		/// Aqui poner para que sirve esta propiedad
		/// </summary>
		public string NOMBRELARGO { get; set; }

		public string NOMBRECORTO { get; set; }

		public string CARACTERISTICAS { get; set; }

		public decimal PRECIODEVENTA { get; set; }

		public string URLIMAGEN { get; set; }

		public bool PUBLICAR { get; set; }

		public bool PRODUCTOENOFERTA { get; set; }

		/// <summary>
		/// Fecha en que inicia la oferta
		/// </summary>
		public DateTime INICIOOFERTA { get; set; }

		/// <summary>
		/// Fecha en la que culmina la oferta
		/// </summary>
		public DateTime FINALOFERTA { get; set; } 

		public decimal PRECIODEOFERTA { get; set; }

		public bool MANEJAEXISTENCIA{ get; set; }  

		public int EXISTENCIA { get; set; }     

		public long TOTALDEVENTAS { get; set; } 

		public List<ProductCategoryLine> oCategorias { get; set; }

		public WooCategoriasProducto oCategoriasProducto { get; set; }

		public Product oProduct { get; set; }

		public ProductImage oProductImage { get; set; }


		public bool EsValidaLaImagen( string URLImagen ) {
			HttpWebRequest request = null;
			HttpWebResponse response = null;
			try {
				request = (HttpWebRequest)WebRequest.Create( URLImagen );
				response = (HttpWebResponse)request.GetResponse();

				Stream StreamImagen = response.GetResponseStream();
				return true;
			} catch (HttpRequestException e) {
				return false;
			} catch (Exception e) {
				return false;
			} finally {
				if (request != null) request.Abort();
				if (response != null) response.Close();
			}
		}
	}
}
