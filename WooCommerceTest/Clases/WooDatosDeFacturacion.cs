﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooDatosDeFacturacion : WooObject {
		public string nombre = "";
		public string apellido = "";
		public string direccion1 = "";

		OrderBilling orderBilling;

		public WooDatosDeFacturacion( OrderBilling orderBilling ) {
			Inicializar( orderBilling );
			CargarDatos();
		}

		private void Inicializar( OrderBilling orderBilling ) {
			this.orderBilling = orderBilling;
		}

		private void CargarDatos() {
			nombre = orderBilling.first_name;
			apellido = orderBilling.last_name;
			direccion1 = orderBilling.address_1;
		}
	}
}
