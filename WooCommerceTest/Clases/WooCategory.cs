﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET;
using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy 
{
	public class WooCategory : WooObject 
	{
		public WooCategory( ) 
		{
			ID = 0;
			NOMBRECATEGORIA = "";
			IDCATEGORIAPADRE = 0;
			MODOVISUALIZACION = WooCategoryDisplay.DEFAULT;

			oProductCategory = new ProductCategory();
		}

		/*
		public WooCategory( ProductCategory pProductCategory ) {
			if (pProductCategory != null) {
				oProductCategory = pProductCategory;

				ID = oProductCategory.id.Value;
				NOMBRECATEGORIA = oProductCategory.name;
				IDCATEGORIAPADRE = (oProductCategory.parent != null) ? oProductCategory.parent.Value : 0;
				MODOVISUALIZACION = oProductCategory.display;
			} else oProductCategory = new ProductCategory();

		}
		*/

		public int ID { get; set; }

		public string NOMBRECATEGORIA { get; set; }

		public int IDCATEGORIAPADRE { get; set; }

		public string MODOVISUALIZACION = WooCategoryDisplay.DEFAULT;

		public ProductCategory oProductCategory { get; set; }

		public WooCategory oCategoriaPadre { get; set; }

		/*
		public void Guardar() 
		{
			oProductCategory.name = NOMBRECATEGORIA;

			if (ID == 0) oProductCategory = wcObject.Category.Add( oProductCategory ).GetAwaiter().GetResult();
			else oProductCategory = (wcObject.Category.Update( ID, oProductCategory, new Dictionary<string, string>() )).Result;
		}
		*/

		public override string ToString()
		{
			return NOMBRECATEGORIA;
		}

		/*
		public Object Tag()
		{
			return this;
		}
		*/
	};
}
