﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET.WooCommerce.v2;

namespace WooCommerceMy {
	public class WooCategoriasProducto : WooObject, IEnumerable 
	{
		public WooCategoriasProducto( ) {
			oProducto = null;
			oCategoriasProducto = new List<WooCategoriaProducto>();
		}

		public WooProducto oProducto { get; set; }

		public List<WooCategoriaProducto> oCategoriasProducto { get; set; }

		/*
		public void CargarTodo() {
			categoriasProducto = new List<WooCategoriaProducto>();
			foreach (ProductCategoryLine productCategory in producto.oProduct.categories) {
				categoriasProducto.Add( new WooCategoriaProducto( productCategory ) );
			}
		}
		*/

		public void LimpiarCategorias() {
			oCategoriasProducto = new List<WooCategoriaProducto>();
		}

		public void AgregarCategoria( int ID ) {
			oCategoriasProducto.Add( new WooCategoriaProducto() { ID = ID } );
		}

		public IEnumerator GetEnumerator() {
			return oCategoriasProducto.GetEnumerator();
		}
	}
}
