﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WooCommerceNET.WooCommerce.v2;


namespace WooCommerceMy {
	public class WooOrden : WooObject {
		public int id;
		public string numeroOrden;
		public string Estado;               // pending, processing, on-hold, completed, cancelled, refunded, failed and trash. Default is pending
		public string moneda;               // https://woocommerce.github.io/woocommerce-rest-api-docs/#order-properties
		public DateTime FechaDeCreacion;
		public decimal descuentoTotal;
		public decimal descuentoImpuesto;
		public decimal totalEnvio;
		public decimal impuestoEnvio;
		public decimal total;
		public decimal impuestoTotal;
		public int idCliente;
		public string notaCliente;

		public WooDatosDeFacturacion datosDeFacturacion = null;
		public WooDetallesDeLaOrden detallesDeLaOrden = null;
		public Order order = null;

		public WooOrden() {
			Inicializar( null );
		}

		public WooOrden( Order order ) {
			Inicializar( order );
			CargarDatos();
		}

		private void Inicializar( Order order ) {
			this.order = order;
		}

		private void CargarDatos() {
			id = order.id.Value;
			numeroOrden = order.number;
			Estado = order.status;
			moneda = order.currency;
			FechaDeCreacion = (order.date_created == null) ? DateTime.Now : order.date_created.Value;
			descuentoTotal = (order.discount_total == null) ? 0 : order.discount_total.Value;
			descuentoImpuesto = (order.discount_tax == null) ? 0 : order.discount_tax.Value;
			totalEnvio = (order.shipping_total == null) ? 0 : order.shipping_total.Value;
			impuestoEnvio = (order.shipping_tax == null) ? 0 : order.shipping_tax.Value;
			total = (order.total == null) ? 0 : order.total.Value;
			impuestoTotal = (order.total_tax == null) ? 0 : order.total_tax.Value;
			idCliente = (order.customer_id == null) ? 0 : order.customer_id.Value;
			notaCliente = order.customer_note;

			datosDeFacturacion = new WooDatosDeFacturacion( order.billing );
			detallesDeLaOrden = new WooDetallesDeLaOrden( order );
		}

		public void Cargar( int id ) {
			order = (wcObject.Order.Get( id )).Result;
			if (order != null)
				CargarDatos();
			else
				Inicializar( null );
		}

		public void Guardar() {
			order.id = id;
			order.number = numeroOrden;
			order.status = Estado;
			order.currency = moneda;
			order.date_created = FechaDeCreacion;
			order.discount_total = descuentoTotal;
			order.discount_tax = descuentoImpuesto;
			order.shipping_total = totalEnvio;
			order.shipping_tax = impuestoEnvio;
			order.total = total;
			order.total_tax = impuestoTotal;
			order.customer_id = idCliente;
			order.customer_note = notaCliente;

			if (id == 0) {
				order = wcObject.Order.Add( order ).GetAwaiter().GetResult();
				id = order.id.Value;
			} else order = wcObject.Order.Update( id, order, new Dictionary<string, string>() ).GetAwaiter().GetResult();
			CargarDatos();
		}
	}
}
